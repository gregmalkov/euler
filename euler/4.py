'''
Created on Aug 2, 2014

@author: Greg Malkov

A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 x 99.

Find the largest palindrome made from the product of two 3-digit numbers.

'''


def ispal(n):
    """Determines if n is a palindrome"""
    for i in range(0, (len(str(n)) // 2)):
        if str(n)[i] != str(n)[(len(str(n)) - i) - 1]:
            return 0
    return 1


n = 0
for i in range(100, 999):
    for j in range(100, 999):
        m = i * j
        if (ispal(m) and m > n):
            n = m

print(str(n))
'''
Created on Aug 16, 2014

@author: Greg Malkov

The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.

'''

def eSieve(target):
	top = int(target ** 0.5 + 1)
	sieve = [True] * target
	product = 1
	for i in range(2, top):
		if sieve[i]:
			product *= i
			for j in range(i * 2, target, i):
				sieve[j] = False
	
	primes = []
	for i in range(2, len(sieve)):
		if(sieve[i]):
			primes.append(i)
	
	return primes


print (sum(eSieve(2000000)))
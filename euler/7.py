'''
Created on Aug 2, 2014

@author: Greg Malkov

By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?

'''

target = 10001;
 
n = 1;
num = 2;
primes = [2];

while (n < target):
    found = 0;
    i = 1;
    while (not found):
        found = 1;
        for prime in primes:
            if(not ((num + i) % prime)):
                found = 0;
                i+=1;
                break;
    n+=1;
    num = num + i;
    primes.append(num);
    
print (str(num));
'''
Created on Aug 14, 2014

@author: Greg Malkov

2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

'''

primes = 2*3*5*7*11*13*17*19
nonPrimes = [4,6,8,9,10,12,14,15,16,18,20]

n = primes

while True:
    notFound = 0
    for x in nonPrimes:
        if(n%x):
            notFound = 1
            break
    if(not notFound):
        break

    n = n + primes

print(str(n))
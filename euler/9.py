'''
Created on Aug 16, 2014

@author: Greg Malkov

A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a2 + b2 = c2
For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.

'''
    
for n in range (1,1000):
    for m in range (n,1000):
        for k in range(1,100):
            a = k*(m*m - n*n)
            b = k*2*m*n
            c = k*(m*m + n*n)
            if(a+b+c == 1000):
                print(str(a*b*c))